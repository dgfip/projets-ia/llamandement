FROM nvidia/cuda:12.2.0-devel-ubuntu20.04
ENV https_proxy=http://proxy.infra.dgfip:3128
ENV http_proxy=http://proxy.infra.dgfip:3128
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y && apt-get install -y python3.9 python3.9-distutils curl python3.9-dev git
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python3.9 get-pip.py
# Copiez le répertoire FastChat dans le conteneur Docker
COPY ./FastChat /FastChat
# Allez dans le répertoire FastChat et installez à partir de ce répertoire
WORKDIR /FastChat
RUN pip3 install -e ".[model_worker]"
RUN pip3 install -e ".[train]"
# RUN pip3 install accelerate
RUN DS_BUILD_CPU_ADAM=1 pip3 install deepspeed

# nvidia/cuda:12.2.0-runtime-ubuntu20.04 docker pull nvidia/cuda:12.2.0-devel-ubuntu20.04
