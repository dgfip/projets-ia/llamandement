# LLaMandement : Grands modèles de langage pour la synthèse de propositions législatives

<p align="center">
    <img src="assets/logo.png" width="30%">
</p>

LLaMandement est un grand modèle de langage (LLM) à l'état de l'art, soumis a un fine-tuning par les agents du Gouvernement afin d'améliorer l'efficacité du traitement des travaux parlementaires français (dont notamment la rédaction des fiches de banc et les travaux préparatoires des réunions interministérielles) grâce à la production de résumés neutres des projets et propositions de loi. LLaMandement est une réponse à la problématique de la gestion d'un nombre d'amendements toujours plus important par les agents de l'administration française. Ce projet représente une avancée technologique significative, en proposant une solution permettant de dépasser l'efficacité et la capacité d'adaptation des agents administratifs à la réalisation d'un travail toujours plus conséquent et de plus en plus difficilement réalisable dans une durée limitée par un humain, tout en offrant la fiabilité et la précision d'un rédacteur spécialisé.

## Détails du Modèle

- **Développé par:** [Direction Générale des Finances Publiques](https://www.impots.gouv.fr/presentation-de-la-dgfip-overview-dgfip) : 
- **Type de modèle:** Un modèle de langue auto-régressif basé sur l'architecture transformers
- **Licence:** Llama 2 Community License Agreement
- **Basé sur le modèle:** [Llama 2](https://arxiv.org/abs/2307.09288)
- **Papier:** [Rapport technique](https://arxiv.org/abs/2401.16182)

## Installation

### Méthode 1 : Avec pip

```bash
cd FastChat
pip3 install "fschat[model_worker,webui,train]"
```
Cette commande vous permet d'installer FastChat, indispensable pour charger et effectuer une inférence avec notre modèle.

### Méthode 2 : Docker

```bash
docker build -t fastchat:training-latest .
# puis
docker run -it --gpus '"device=0,1"' -v /path/to/model:/path/to/model fastchat:training-latest # with gpu
docker run -it -v /path/to/model:/path/to/model fastchat:training-latest # no gpu
```
Ici, nous construisons une image Docker et exécutons un conteneur. Ce processus nécessite Docker d'installé dans votre système et il est recommandé si vous prévoyez d'utiliser le modèle à grande échelle ou dans un environnement de production. 

Pour installer Docker, vous pouvez consulter la documentation officielle de Docker à l'adresse suivante : [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)


## Commencer avec LLamadement

Les poids pré-entraînés du modèle LLamadement sont disponibles sur Hugging Face à l'adresse suivante : [LlaMAndement-7b](https://huggingface.co/ActeurPublic/LlaMAndement-7b) et [LlaMAndement-13b](https://huggingface.co/ActeurPublic/LlaMAndement-13b).   

### Inférence avec l'interface de ligne de commande

```bash
python3.9 -m fastchat.serve.cli --model-path /paht/to/llamadement --conv-template alpaca ##--device cpu if no gpu available
```

<p align="left">
    <img src="assets/cli_llamandement.png" width="50%">
</p>
Cette commande vous permet de démarrer le modèle et de commencer à l'interroger.

## Re-Entraîner LLaMAndement

Les données d'entraînement qui ont servi à apprendre à LLamandement se trouvent dans ./data/fine_tuning_data_dila_v4.json.
Pour plus d'informations sur la composition du dataset, voir la section 3.3 du [rapport technique](https://arxiv.org/abs/2401.16182)

Si vous souhaitez ré-entraîner LLaMAndement, voici les étapes à suivre : 

1. Téléchargez llama v2 13b : https://huggingface.co/meta-llama/Llama-2-13b-chat-hf

2. Configurez train_llamandement_13b.sh

3. Lancez un fine tuning

Il est recommandé de lancer le script d'entraînement dans une image Docker pour assurer un environnement de développement cohérent et éviter les problèmes de dépendances. En utilisant une image Docker, vous pouvez encapsuler toutes les dépendances nécessaires, y compris les versions spécifiques des bibliothèques et des frameworks, ce qui facilite la reproductibilité de l'entraînement du modèle.

```bash
sh train_llamandement_13b.sh
```

## Citation
```
@article{gesnouin2024llamandement,
  title={LLaMandement: Large Language Models for Summarization of French Legislative Proposals},
  author={Gesnouin, Joseph and Tannier, Yannis and Da Silva, Christophe Gomes and Tapory, Hatim and Brier, Camille and Simon, Hugo and Rozenberg, Raphael and Woehrel, Hermann and Yakaabi, Mehdi El and Binder, Thomas and others},
  journal={arXiv preprint arXiv:2401.16182},
  year={2024}
}
```